var express = require('express');
var router = express.Router();

const users = require("../users.json");

router.get('/', function(req, res, next) {
  res.send(users);
}
);

router.get('/:id', function(req, res, next) {
  let userId = Number(req.params.id.replace(/\//, ''));
  if(Number.isInteger(userId) && userId > 0) {
    res.send(users[userId - 1]);
  } else {
    res.status(400).send('ID is not correct');
  }
}
);


module.exports = router;
